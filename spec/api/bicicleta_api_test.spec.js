var Bicicleta = require('../../models/bicicleta_model');

var request  = require('request');
var server = require('../../bin/www');

beforeEach(function(){console.log('testeando…'); });

describe('Bicicleta Api', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1,'negro', 'urbana', [-17.797776, -63.175235])
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        })
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
           var headers = {'content-type': 'application/json'};
           var aBici = '{"id": 10, "color":"negro","modelo":"urbana", "lat": -17.797776,"lng":-63.175235}';

           request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
                } ,function(error,response,body){
                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.findById(10).color).toBe("negro");
                    done();    
                }
            );
        });
    });
});

