var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta_model');

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});
      //  {useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true}

        const db = mongoose.connection;
        db.on('open', function(){
            console.log('Conectado a la database testbd!!');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    //primer test
    describe('Bicicleta.createInstance', ()=>{
        it('crea una instancia de bicicleta', ()=>{
            var bici = Bicicleta.createInstance(1,"verde","urbana",[-17.797776, -63.175235]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-17.797776);
            expect(bici.ubicacion[1]).toEqual(-63.175235);
        });
    });

    describe('Bicicleta.allBicis', ()=>{
        it('Comienza vacia', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', ()=>{
        it('Agrego solo una bici', (done)=>{
            var aBici = new Bicicleta({code:1,color:"verde", modelo:"urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                })
            });
        });
    });

    describe('Bicicleta.findByCode', ()=>{
        it('debe devolver la bici con el code 1', (done)=>{
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code:1,color:"negro", modelo:"urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code:2,color:"rojo", modelo:"urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                    if(err) console.log(err);
                    Bicicleta.findByCode(1, function(error, targetBici){
                        expect(targetBici.code).toBe(aBici.code);
                        expect(targetBici.color).toBe(aBici.color);
                        expect(targetBici.modelo).toBe(aBici.modelo);

                        done();
                    });                   
                });
                });
            });
        });
    });
});



// beforeEach(()=>{Bicicleta.allBicis = []; });

// describe('Bicicleta.allBicis', ()=>{
//    it('comienza vacia', ()=>{
//        expect(Bicicleta.allBicis.length).toBe(0);
//    }); 
// });

// describe('Bicicleta.add',()=>{
//     it('agregamos una', ()=>{
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var bici = new Bicicleta(1,'rojo','sport', [-17.797776, -63.175235]);
//         Bicicleta.add(bici);
//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(bici);
//     });
// });

// describe('Bicicleta.findById', ()=>{
//     it('debe devolver la bici con id 1',()=>{
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var aBici1 = new Bicicleta(1,'rojo','montaña');
//         var aBici2 = new Bicicleta(2,'azul','sport');
//         Bicicleta.add(aBici1);
//         Bicicleta.add(aBici2);

//         var targetBici = Bicicleta.findById(1);
//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(aBici1.color);
//         expect(targetBici.modelo).toBe(aBici1.modelo);
//     });
// });