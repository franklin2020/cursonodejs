var mymap = L.map('main_map').setView([-17.793142, -63.174089], 15);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {

attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',

}).addTo(mymap);




//llamada ajax, req asincronico
$.ajax({
    dataType:"json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            var marker = L.marker(bici.ubicacion).addTo(mymap);
            marker.bindPopup((bici.id).toString()).openPopup();
           //L.marker(bici.ubicacion,{title: "hola"}).addTo(myMap);
        });
    }
})