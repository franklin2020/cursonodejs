var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//comenzamos a crear nuestro modelo con Schema
var bicicletaSchema  = Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: {type:'2dsphere', sparse: true}
    }
})

//ahora creamos los metodos
//create instance
bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
}
// Bicicleta.findById = function(aBiciId){
    //     var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    
    //     if(aBici)
    //         return aBici;
    //     else
    //         throw new Error(`No existe la Bicicleta con el id: ${aBiciId}`);    
    // }
    //---------------------------------------------------------
    
// //metodo toString
bicicletaSchema.methods.toString = function(){
    return 'code: '  + this.code + '| color: ' + this.color;
}
// Bicicleta.prototype.toString = function(){
    //     return 'id:' + this.id + "| color: " + this.color;
// }
//------------------------------------------------------
//metodo listar bicis
bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
}
// Bicicleta.allBicis = [];
// Bicicleta.add = function(aBici){
//     Bicicleta.allBicis.push(aBici);
// }
bicicletaSchema.statics.add  = function(aBici, cb){
    this.create(aBici, cb);
}


bicicletaSchema.statics.findByCode = function(aCode, cb){
    return this.findOne({code: aCode}, cb);
}

bicicletaSchema.statics.removeByCode = function(aCode, cb){
    return this.deleteOne({code: aCode}, cb);
}

// var Bicicleta = function(id, color, modelo, ubicacion){
//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;
// }


// //metodo remover
// Bicicleta.removeById = function(aBiciId){
    //  for (var i = 0; i < Bicicleta.allBicis.length; i++) {
        //      if(Bicicleta.allBicis[i].id == aBiciId){
            //          Bicicleta.allBicis.splice(i,1);
            //          break;
            //      }
            
            //  }
            // }
            
            
            //creamos 2 objetos de tipo bicicleta
            /*var b1 = new Bicicleta(1, 'rojo', 'urbana', [-17.797776, -63.175235]);
            var b2 = new Bicicleta(2,'azul', 'montañera', [-17.800504, -63.180131]);
            
            Bicicleta.add(b1);
            Bicicleta.add(b2);*/
//exportamos el modelo
//module.exports = Bicicleta;

            //ahora lo exportamos
module.exports  = mongoose.model('Bicicleta', bicicletaSchema);